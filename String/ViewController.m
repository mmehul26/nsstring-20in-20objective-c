//
//  ViewController.m
//  String
//
//  Created by Mehul Makwana on 01/09/18.
//  Copyright © 2018 Mehul Makwana. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"Literal syntax for NSString %@", @"str1");
    
    NSLog(@"NSMutableString with stringWithString Method %@",[NSMutableString stringWithString:@"str2"]);
    
    NSLog(@"NSMutableString with Literal Syntax %@",[@"str3" mutableCopy]);
    
    NSLog(@"string with formate method %@",[NSMutableString stringWithFormat:@"str4 with number %d",4]);
    
    NSLog(@"Lenth of string %lu",(unsigned long)@"str5".length);
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
}


@end
